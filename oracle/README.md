
Oracle Database 12c Enterprise Edition requires authentication

```
$ docker login
WARNING! Your password will be stored unencrypted in /home/centos/.docker/config.json
```

Get the image from Hub

```
$ docker pull store/oracle/database-enterprise:12.2.0.1
```

Run the container

```
$ docker run -d –P –it -v /mnt/data/OracleDBData:/ORCL --name myoradb --env-file ora.conf store/oracle/database-enterprise:12.2.0.1
```

Jump into the container

```
$ docker exec -it myoradb bash -c "source /home/oracle/.bashrc; sqlplus /nolog“
```

Watch their logs

```
$ docker logs myoradb
```

Check its listening port

```
$ docker port myoradb
```

# Credits to
[Oracle Database](https://hub.docker.com/_/oracle-database-enterprise-edition)

