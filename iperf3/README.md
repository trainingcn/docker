# Start the server

Using an ad-hoc command (interactive mode):

```docker run --rm --name iperf3srv -it iperf3 -s```

Using an ad-hoc command (dettached mode):

```docker run --rm --name iperf3srv -d iperf3```

Using a Docker Compose definition file:

```
docker-compose up -d
docker-compose ps
```

# Start the client

Look at the IP address of the listening server:

```docker inspect --format "{{ .NetworkSettings.IPAddress }}" iperf3srv```

Run iPerf3 client:

```docker run --rm --name my-iperf3 -it iperf3 -c <SRVIPADDR>```

# Credits to

iPerf3 image provided by [NetworkStatic](https://hub.docker.com/r/networkstatic/iperf3/)
